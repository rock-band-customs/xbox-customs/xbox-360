# :guitar: THE ROCK BAND CUSTOMS PROJECT 

This GitLab repository is part of the **Rock Band Customs Project** founded by Harvey Houston (HarvHouHacker). It is a collection of software designed to be used for the consoles mentioned in the project title. For info about proper usage, copyright, and other information, please see the [Terms and Conditions](https://gitlab.com/-/snippets/2068521) before using ANY of the software provided here.

# :video_game: ROCK BAND CUSTOMS FOR XBOX 360 - ONE

Coming soon.
